'use strict';

module.exports = function(gulp, cfg, libs) {
  var webSrc = cfg.src;
  var webDst = cfg.dst;
  gulp.task('www-clean', function(done) {
    libs.del(webDst + '**/*.*').then(function(paths) {
      libs.deleteEmpty(webDst, function(err, deleted) {
        done(err);
      });
    });
  });
  gulp.task('www-jshint', function() {
    var jshint = libs.jshint;
    var src = [
      webSrc + 'app/**/*.js'
    ];
    return gulp
      .src(src)
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'), {verbose:true})
      .pipe(jshint.reporter('fail'));
  });
  gulp.task('www-index', ['www-clean'], function() {
    return gulp.src(webSrc + 'index.html')
    .pipe(gulp.dest(webDst));
  });
  gulp.task('www-index-args', [cfg.last], function(done) {
    var args = cfg;
    var keys = Object.keys(args);
    var file = webDst + 'index.html';
    var temp = cfg.tmp + 'index.html';
    libs.fs.readFile(file, 'utf8', processFile);
    function processFile(err, data) {
      if (err) {return done(err);}
      var contents = data;
      keys.forEach(function(key) {
        var search = '${' + key + '}';
        var replace = args[key];
        contents = contents.split(search).join(replace);
      });
      //console.log(contents);
      libs.fs.writeFile(temp, contents, 'utf8', moveFile);
    }
    function moveFile(err, data) {
      if (err) {return done(err);}
      libs.fs.rename(temp, file, done);
    }
  });
};
