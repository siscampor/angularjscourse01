'use strict';

module.exports = function(gulp, cfg, libs) {
  var webSrc = cfg.src;
  var webDst = cfg.dst;
  var opt = cfg.optimize;
  var gif = libs.gif;
  var wOptions = {
    bowerJson: require('../bower' + (opt ? '' : '') + '.json'),
    directory: webSrc + '.bower_components',
    ignorePath: webSrc,
    fileTypes: {
      html: {
        replace: {
          css: function(filepath) {
            return '<link rel="stylesheet" href="${path}/' + filepath.slice(1) +
              '?v=${now}" /><!-- wiredep -->';
          },
          js: function(filepath) {
            return '<script type="text/javascript" src="${path}/' + filepath.slice(1) +
              '?v=${now}"></script><!-- wiredep -->';
          }
        }
      }
    }
  };
  var iOptions = {
    ignorePath: [webSrc, webDst],
    relative: true,
    addRootSlash: false,
    transform: function(filepath, file, start, length, e) {
      //console.log('filepath: ' + Object.keys(file).join(' '));
      var ext = filepath.slice(filepath.lastIndexOf('.')).toLowerCase();
      //console.log('filepath: ' + filepath + ' ' + ext);
      switch (ext) {
        case '.css':
          return '<link rel="stylesheet" href="${path}/' + filepath +
            '?v=${now}"/><!-- inject -->';
        default:
          var filename = (filepath.slice(1, webSrc.length + 1) === webSrc) ?
            filepath.slice(webSrc.length + 1) : filepath;
          return '<script type="text/javascript" src="${path}/' + filename +
            '?v=${now}"></script><!-- inject -->';
      }
    }
  };
  gulp.task('www-inject', [
    'www-inject-wiredep',
    'www-inject-templates'
  ]);
  gulp.task('www-inject-wiredep', ['www-index', 'www-resources', 'www-code'], function() {
    var iSrc = opt ? [
      webSrc + 'js/*.js',
      '!' + webSrc + 'js/*.polyfill.js',
      webDst + 'app/app.js',
      webDst + 'js/build.js'
    ] : [
      webSrc + 'js/*.js',
      '!' + webSrc + 'js/**/*.polyfill.js',
      '!' + webSrc + 'app/**/*.polyfill.js',
      webSrc + 'app/**/*.module.js',
      webSrc + 'app/**/*.js',
      '!' + webSrc + 'app/**/*.spec.js',
      '!' + webSrc + 'app/**/*.test.js'
    ];
    return gulp.src(webDst + 'index.html')
    .pipe(libs.wiredep(wOptions))
    .pipe(libs.inject(
      gulp.src(iSrc, {read: false}),
      iOptions
    ))
    .pipe(gulp.dest(webDst));
  });
  gulp.task('www-inject-styles', ['www-inject-wiredep'], function() {
    return gulp.src(webDst + 'index.html')
    .pipe(libs.inject(
      gulp.src(webDst + 'styles/*.css', {read: false}),
      iOptions
    ))
    .pipe(gulp.dest(webDst));
  });
  gulp.task('www-inject-templates', ['www-inject-styles', 'www-env'], function(done) {
    if (!opt) {
      done();
      return;
    }
    var tOptions = {
      ignorePath: [webSrc, webDst],
      relative: true,
      addRootSlash: false,
      starttag: '<!-- inject:templates:{{ext}} -->',
      transform: function(filepath, file, start, length, e) {
        //console.log('filepath: ' + Object.keys(file).join(' '));
        var ext = filepath.slice(filepath.lastIndexOf('.')).toLowerCase();
        //console.log('filepath: ' + filepath + ' ' + ext);
        var filename = (filepath.slice(1, webSrc.length + 1) === webSrc) ?
          filepath.slice(webSrc.length + 1) : filepath;
        return '<script type="text/javascript" src="${path}/' + filename +
          '?v=${now}"></script><!-- inject -->';
      }
    };
    return gulp.src(webDst + 'index.html')
    .pipe(libs.inject(
      gulp.src(webDst + 'js/templates.js', {read: false}),
      tOptions
    ))
    .pipe(gulp.dest(webDst));
  });
};
