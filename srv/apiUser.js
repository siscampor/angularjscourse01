'use strict';

module.exports = function(app) {

  class ApiUser extends app.ApiModel {
    constructor() {
      super('user');
    }
    findByName(name) {
      var self = this;
      return this.loadFromFile()
      .then(function(list) {
        var index = self.indexOfProp(list, 'username', name);
        return Promise.resolve(index >=0 ? list[index] : null);
      });
    }

  }
  app.ApiUser = ApiUser;
};
