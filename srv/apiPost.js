'use strict';

module.exports = function(app) {

  class ApiPost extends app.ApiModel {
    constructor() {
      super('post');
    }
    route() {
      var api = this.api;
      var mw = app.middleware;
      api.get('/:topic/posts', this.search.bind(this));
      api.get('/:id', this.findById.bind(this));
      api.post('/', mw.bodyJson, this.insert.bind(this));
      api.put('/:id', mw.bodyJson, this.update.bind(this));
      api.del('/:id', this.del.bind(this));
    }
    search(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var topic = req.params.topic | 0;
      return this.loadFromFile()
      .done(function(list) {
        var posts = list.filter(function(post) {
          return post.topic === topic;
        }).filter(function(post, idx) {
          return true; //idx < 20; // limit
        });
        res.json(posts);
      }, next);
    }

  }
  app.ApiPost = ApiPost;
};
