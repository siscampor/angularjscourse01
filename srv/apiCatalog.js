'use strict';
var util = require('util');

module.exports = function(app) {

  class ApiCatalog extends app.ApiModel {
    constructor() {
      super('catalog');
    }
    search(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var title = req.query.title || null;
      var category = req.query.category || null;
      var author = req.query.author || null;
      var description = req.query.description || null;
      return this.loadFromFile()
      .done(function(list) {
        var found = list.filter(function(catalog) {
          return (!title || catalog.title.indexOf(title) >= 0) &&
            (!category || catalog.category === category) &&
            (!author || catalog.author.indexOf(author) >= 0) &&
            (!description || catalog.description.indexOf(description) >= 0);
        }).filter(function(post, idx) {
          return (!title && !category && !author && !description) ? true : idx < 20; // limit
        });
        res.json(found);
      }, next);
    }
  }
  app.ApiCatalog = ApiCatalog;
};
