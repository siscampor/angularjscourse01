'use strict';
var path = require('path'),
	util = require('util'),
	crypto = require('crypto'),
	express = require('express'),
	favicon = require('serve-favicon'),
	logger = require('morgan'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	session = require('express-session');

module.exports = function(app) {
  var web = app.web = express();
  var api = app.api = express.Router();
  var dirStatic = path.resolve(app.root, 'www');
  app.middleware = {
    bodyJson: bodyParser.json()
  };
  web.set('etag', false);
  web.use(favicon(path.join('www', 'img', 'favicon', 'favicon.ico')));
  web.use(logger('dev', {
    immediate: false
  }));
  web.use(cookieParser());
  web.use(express.static(dirStatic));
  web.use(session(app.config.session));
  web.use(mwUser);
  web.use(mwNoCache);
  web.use('/api', api);
	web.get('/ui/*', mwPushState);
  web.use(mwNotFound);
  web.use(mwError);

  function mwUser(req, res, next) {
    var user_id = req.session.user;
		req.user = null;
		if (!user_id) {
			return next();
		}
		app.apiUser.loadFromFile()
		.done(function(list) {
			var idx = app.apiUser.indexOfId(list, user_id);
			if (idx >= 0) {
				req.user = list[idx];
			} else {
				req.user = null;
			}
			next();
		}, next);
  }
  function mwPushState(req, res, next) {
    res.sendFile(path.join(dirStatic, 'index.html'));
  }
  function mwNoCache(req, res, next) {
  	res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  	res.header('Pragma', 'no-cache');
  	res.header('Expires', 0);
  	next();
  }
  function mwNotFound(req, res, next) {
    var error = new Error('ERROR_NOT_FOUND');
    error.status = 404;
    console.error(`Route not found ${req.method} ${req.url}`);
    next(error);
  }
  function mwError(err, req, res, next) {
    if (typeof(err) === 'string') {
      err = {
        message: err
      };
    }
    err.status = err.status || 500;
    if (err.status !== 404) {
      console.error(err);
    }
    var error = {
      message: err.message,
      status: err.status
    };
    res.status(error.status);
    res.json(error);
  }
};
