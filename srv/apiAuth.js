'use strict';
var util = require('util');
var path = require('path');
var express = require('express');
var moment = require('moment');
var RSA = require('node-rsa');

module.exports = function(app) {

  class ApiAuth extends app.ApiModel {
    constructor() {
      super('auth');
      this.rsa = new RSA({b:512});
      this.publicPEM = this.rsa.getPublicPEM();
    }
    route() {
      var api = this.api;
      var mw = app.middleware;
      api.get('/me', this.me.bind(this));
      api.get('/pem', this.pem.bind(this));
      api.post('/login', mw.bodyJson, this.login.bind(this));
      api.get('/logout', this.logout.bind(this));
    }
    me(req, res, next) {
      res.json(req.user);
    }
    pem(req, res, next) {
      res.json({key:this.publicPEM});
    }
    login(req, res, next) {
      var self = this;
      var errorMessage = 'Usuario y/o Contraseña no válidos';
      var username = req.body.username;
      var password = this.rsa.decrypt(req.body.password).toString();
      console.log(util.format('login: %s', username));
      if (!username || !password) {
        return next(new Error(errorMessage));
      }
      app.apiUser.loadFromFile()
      .then(function(list) {
        var found = list.filter(function(user) {
          return user.username === username && user.password === password;
        });
        console.log('login found: ' + !!found.length);
        if (found.length) {
          var user = found[0];
          req.session.user = user.id;
          res.json(user);
        } else {
          req.session.user = null;
          var error = new Error(errorMessage);
          error.status = 403;
          next(error);
        }
      }, next);
    }
    logout(req, res, next) {
      req.session.destroy(function(err) {
        if (err) {
          return next(err);
        } else {
          res.redirect('/');
        }
      });
    }

  }
  app.ApiAuth = ApiAuth;
};
