'use strict';
var fs = require('fs'),
  path = require('path');

module.exports = function(app) {
  var db = app.db = {
    user: require('../db/user.json'),
    post: require('../db/post.json'),
    topic: require('../db/topic.json'),
    save: saveJson,
    findById: findById,
    filterByProp: filterByProp
  };

  function saveJson(dbName) {
    var json = JSON.stringify(db[dbName], null, '\t');
    var filename = path.resolve(app.root, 'db', `${dbName}.json`);
    fs.writeFileSync(filename, json, 'utf8');
  }
  function findById(dbName, id) {
    var list = db[dbName];
    for(var k=0,n=list.length; k<n; k++) {
      var item = list[k];
      if (item.id === id) {
        return item;
      }
    }
    return null;
  }
  function filterByProp(dbName, name, value) {
    var list = db[dbName];
    return list.filter(function(item) {
      return item[name] === value;
    });
  }
};
