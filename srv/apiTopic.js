'use strict';
var util = require('util');

module.exports = function(app) {

  class ApiTopic extends app.ApiModel {
    constructor() {
      super('topic');
    }
    search(req, res, next) {
      return this.loadFromFile()
      .then(function(list) {
        var topics = {};
        list.forEach(function(topic) {
          topics['id' + topic.id] = topic;
          topic.posts = 0;
        });
        return app.apiPost.loadFromFile()
        .then(function(posts) {
          posts.forEach(function(post) {
            var topic = topics['id' + post.topic];
            if (!topic) {
              console.error('topic not found\n' + util.inspect(post));
              return;
            }
            topic.posts += 1;
          });
          return list;
        });
      })
      .done(function(list) {
        res.json(list);
      }, next);
    }

  }
  app.ApiTopic = ApiTopic;
};
