'use strict';

module.exports = function(app) {

  app.config.session = {
  	secret: 'AngularJS Course',
  	rolling: false,
  	resave: false,
  	saveUninitialized: false,
    cookie: {
      path: '/',
      httpOnly: true,
      secure: false,
      maxAge: null
    }
  };
  require('./promise.js')(app);
  require('./express.js')(app);
  require('./api.js')(app);
};
