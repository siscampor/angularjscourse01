'use strict';
var fs = require('fs'),
  path = require('path'),
  moment = require('moment'),
  express = require('express');

module.exports = function(app) {

  class ApiModel {
    constructor(dbName) {
      this.dbName = dbName;
      this.db = null;
      this.filename = path.join(app.root, 'db', `${dbName}.json`);
      this.api = express.Router();
      app.api.use(`/${this.dbName}`, this.api);
      this.api.del = this.api['delete'].bind(this.api);
      this.route();
    }
    route() {
      var api = this.api;
      var mw = app.middleware;
      api.get('/', this.search.bind(this));
      api.get('/:id', this.findById.bind(this));
      api.post('/', mw.bodyJson, this.insert.bind(this));
      api.put('/:id', mw.bodyJson, this.update.bind(this));
      api.del('/:id', this.del.bind(this));
    }
    loadFromFile() {
      if (this.db) {
        return Promise.resolve(this.db);
      }
      var self = this;
      return new Promise(function(resolve, reject) {
        fs.readFile(self.filename, 'utf8', function(err, content) {
          if (err) {
            reject(err);
          } else {
            var list = JSON.parse(content || '[]');
            self.db = list;
            resolve(list);
          }
        });
      });
    }
    saveToFile() {
      var self = this;
      return new Promise(function(resolve, reject) {
        fs.writeFile(self.filename, JSON.stringify(self.db, null, '\t'), 'utf8', function(err) {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    }
    indexOfId(list, id) {
      for(var k=0,n=list.length; k<n; k++) {
        var item = list[k];
        if (item.id === id) {return k;}
      }
      return -1;
    }
    indexOfProp(list, name, value) {
      for(var k=0,n=list.length; k<n; k++) {
        var item = list[k];
        if (item[name] === value) {return k;}
      }
      return -1;
    }
    compareIds(a, b) {
      return a.id - b.id;
    }
    search(req, res, next) {
      return this.loadFromFile()
      .done(function(list) {
        res.json(list);
      }, next);
    }
    findById(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = req.params.id | 0;
      return this.loadFromFile()
      .done(function(list) {
        var index = self.indexOfId(list, id);
        var item = index >= 0 ? list[index] : null;
        console.log({id:id,index:index});
        res.json(item);
      }, next);
    }
    insert(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = moment().format('YYYYMMDDHHmmss') | 0;
      return this.loadFromFile()
      .then(function(list) {
        var body = req.body;
        body.id = id;
        list.push(body);
        list.sort(self.compareIds);
        return self.saveToFile(list);
      }).done(function() {
        res.json({id:id});
      }, next);
    }
    update(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = req.params.id | 0;
      return this.loadFromFile()
      .then(function(list) {
        var index = self.indexOfId(list, id);
        if (index >= 0) {
          list[index] = req.body;
          return self.saveToFile(list);
        } else {
          var error = new Error('NOT_FOUND');
          error.status = 404;
          return Promise.reject(error);
        }
      }).done(function() {
        res.json({id:id});
      }, next);
    }
    del(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = req.params.id | 0;
      return this.loadFromFile()
      .then(function(list) {
        var index = self.indexOfId(list, id);
        if (index >= 0) {
          list.splice(index, 1);
          return self.saveToFile(list);
        }
        return Promise.resolve(null);
      }).done(function() {
        res.json({id:id});
      }, next);
    }

  }
  app.ApiModel = ApiModel;
};
