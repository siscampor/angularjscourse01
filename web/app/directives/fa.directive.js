/**
* FontAwesome Directive
**/
(function() {
  'use strict';
  angular.module('app.directives')
  .directive('fa', faDirective);

  function faDirective() {
    return {
      restrict: 'A',
      scope: {
        fa: '@'
      },
      link: function(scope, elem, attrs) {
        elem.addClass('fa');
        scope.$watch('fa', watchFa);

        function watchFa(newValue, oldValue) {
          if (oldValue) {
            elem.removeClass('fa-' + oldValue);
          }
          if (newValue) {
            elem.addClass('fa-' + newValue);
          }
        }
      }
    };
  }
})();
