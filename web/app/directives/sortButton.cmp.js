/**
* Sort Button Directive
**/
(function() {
  'use strict';
  angular.module('app.directives')
  .component('sortButton', {
    templateUrl: templateUrl('app/directives/sortButton.tpl.html'),
    controller: SortButtonController,
    controllerAs: 'ctrl',
    bindings: {
      buttonClass: '@',
      field: '@',
      sortName: '=',
      sortDir: '='
    }
  });
  SortButtonController.$inject = ['$scope'];
  function SortButtonController($scope) {
    var ctrl = this;
    ctrl.doSort = doSort;
    $scope.$watch('ctrl.sortName', changedSort);
    $scope.$watch('ctrl.sortDir', changedSort);
    changedSort();

    function changedSort() {
      var dirClass = ctrl.sortDir ? 'fa-sort-desc' : 'fa-sort-asc';
      ctrl.iconClass = ctrl.sortName === ctrl.field ? dirClass : 'fa-sort';
    }
    function doSort() {
      if (ctrl.sortName === ctrl.field) {
        ctrl.sortDir = !ctrl.sortDir;
      } else {
        ctrl.sortName = ctrl.field;
        ctrl.sortDir = false;
      }
    }
  }
})();
