/**
* Bounce Directive
**/
(function() {
  'use strict';
  angular.module('app.directives')
  .directive('bounce', bounceDirective);

  bounceDirective.$inject = ['$timeout'];
  function bounceDirective($timeout) {
    return {
      restrict: 'A',
      link: BounceLink
    };
    function BounceLink(scope, elem, attrs) {
      /* jshint bitwise: false */
      var timeout = 1000;
      if (attrs.bounce) {
        timeout = attrs.bounce | 0;
      }
      elem.on('click', onClick);

      function onClick(e) {
        elem[0].setAttribute('disabled', '');
        $timeout(onTimeout, timeout);
      }
      function onTimeout() {
        elem[0].removeAttribute('disabled');
      }
    }
  }
})();
