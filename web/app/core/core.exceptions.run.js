/**
* Exceptions
**/
(function() {
  'use strict';
  angular.module('app.core')
  .run(runExceptions);

  runExceptions.$inject = ['$rootScope'];
  function runExceptions($rootScope) {
    $rootScope.errorMessage = getErrorMessage;
    $rootScope.showInfo = showInfo;
    $rootScope.showWarn = showWarn;
    $rootScope.showError = showError;
    $rootScope.fcnInfo = fcnShow(showInfo);
    $rootScope.fcnWarn = fcnShow(showWarn);
    $rootScope.fcnError = fcnShow(showError);

    function getErrorMessage(err) {
      if (!err) {return 'ERROR';}
      if (err.data && err.data.message) { return err.data.message; }
      if (typeof(err.data) === 'string') { return err.data; }
      if (err.message) { return err.message; }
      return err.toString();
    }
    function showInfo(err, title) {
      $rootScope.toastr.info(getErrorMessage(err), title);
    }
    function showWarn(err, title) {
      $rootScope.toastr.warning(getErrorMessage(err), title);
    }
    function showError(err, title) {
      $rootScope.toastr.error(getErrorMessage(err), title);
    }
    function fcnShow(fcn) {
      return function fcnShowDef(title) {
        return function fcnShowImpl(err) {
          fcn(err, title);
        };
      };
    }
  }
})();
