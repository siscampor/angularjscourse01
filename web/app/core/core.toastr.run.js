/**
* Toaster Run
**/
(function() {
  'use strict';
  /* globals toastr */
  angular.module('app.core')
  .run(runToastr);

  runToastr.$inject = ['$rootScope'];
  function runToastr($rootScope) {
    $rootScope.toastr = toastr;
    toastr.options.escapeHtml = true;
    toastr.options.progressBar = true;
  }
})();
