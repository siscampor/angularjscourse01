/**
* Auth Run
**/
(function() {
  'use strict';
  angular.module('app.core')
  .run(runAuth);

  runAuth.$inject = ['$rootScope', 'AuthService'];
  function runAuth($rootScope, AuthService) {
    /* jshint devel:true */
    $rootScope.getAuth = getAuth;
    $rootScope.setAuth = setAuth;
    getAuth().then(null, function(err) {
      console.error('auth', err);
    });

    function getAuth() {
      return AuthService.me()
      .then(function(data) {
        $rootScope.setAuth(data);
        return data;
      });
    }
    function setAuth(data) {
      console.log('auth-changed', data);
      AuthService.user = AuthService.auth = data;
      $rootScope.$broadcast('auth-changed', data);
    }
  }
})();
