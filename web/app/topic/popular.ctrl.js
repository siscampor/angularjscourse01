/**
* Popular Controller
**/
(function() {
  'use strict';
  angular.module('app.topic')
  .controller('TopicPopularCtrl', TopicPopularCtrl);

  TopicPopularCtrl.$inject = ['$scope', 'TopicService', 'topics'];
  function TopicPopularCtrl($scope, TopicService, topics) {
    var vm = this;
    vm.topics = topics;
    vm.search = '';
    vm.sortAsc = false;
    vm.sortDsc = true;
    vm.sortName = 'title';
    vm.sortDir = vm.sortAsc;
    vm.clsBtnSort = 'btn-primary';
    vm.update = update;
    //update();

    function update() {
      TopicService.list()
      .then(function(list) {
        vm.topics = list;
      }, $scope.fcnError('Topicos Populares'));
    }
  }
})();
