/**
* Topic List Controller
**/
(function() {
  'use strict';
  angular.module('app.topic')
  .controller('TopicListCtrl', TopicListCtrl);

  TopicListCtrl.$inject = ['$scope', 'TopicService'];
  function TopicListCtrl($scope, TopicService) {
    var vm = this;
    vm.topics = [];

    TopicService.list()
    .then(function(list) {
      vm.topics = list;
    }, $scope.fcnError('Categorias'));
  }
})();
