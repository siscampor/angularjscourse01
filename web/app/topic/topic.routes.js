/**
* Routes
**/
(function() {
  'use strict';
  angular.module('app.topic')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .abs('app.topic', '/topic')
    .when('app.topic.list', '/', {
      template: '<view-topic-list topics="$resolve.topics"></view-topic-list>',
      title: 'Categorías',
      resolve: {
        topics: topicsResolve,
        //delay: delayResolve,
        startLoading: startLoading
      }
    })
    .when('app.topic.populares', '/populares', {
      templateUrl: templateUrl('app/topic/popular.view.html'),
      controller: 'TopicPopularCtrl as vm',
      title: 'Topicos Populares',
      resolve: {
        topics: topicsResolve
      }
    });

    topicsResolve.$inject = ['TopicService'];
    function topicsResolve(TopicService) {
      return TopicService.list();
    }

    delayResolve.$inject = ['$timeout'];
    function delayResolve($timeout) {
      return $timeout(2000);
    }

    startLoading.$inject = ['$rootScope', '$timeout'];
    function startLoading($rootScope, $timeout) {
      $rootScope.isLoadingRoute = true;
      return $timeout(0);
    }
  }
})();
