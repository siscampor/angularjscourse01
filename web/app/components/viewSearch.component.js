/**
* View Search Component
**/
(function() {
  'use strict';
  angular.module('app.components')
  .component('viewSearch', {
    templateUrl: templateUrl('app/components/viewSearch.component.html'),
    bindings: {
      search: '<',
      onSearch: '&'
    }
  });
})();
