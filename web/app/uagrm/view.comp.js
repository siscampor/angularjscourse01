/**
* View Component
**/
(function() {
  'use strict';
  angular.module('app.uagrm')
  .component('viewUagrmView', {
    templateUrl: templateUrl('app/uagrm/view.comp.html'),
    controller: ViewCtrl,
    bindings: {
      topic: '<',
      posts: '<'
    }
  });

  ViewCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$uibModal',
    'PostService',
    'TopicService'];
  function ViewCtrl(
    $scope, $rootScope, $uibModal,
    PostService, TopicService) {
    var $ctrl = this;
    $ctrl.isShowingPanel = false;
    $ctrl.panelModel = newTopic();
    $ctrl.like = like;
    $ctrl.$onInit = $onInit;
    $ctrl.showPanel = showPanel;
    $ctrl.hidePanel = hidePanel;
    $ctrl.savePanel = savePanel;
    $ctrl.showDialog = showDialog;

    function $onInit() {
      //console.log('ViewCtrl', $ctrl);
    }
    function updatePosts() {
      return PostService.list($ctrl.topic.id)
      .then(function(posts) {
        $ctrl.posts = posts;
        return posts;
      });
    }
    function like(post) {
      post.likes += 1;
      PostService.update(post)
      .then(function(data) {
        return updatePosts();
      })
      .then(function() {
      }, $rootScope.fcnError('Post Like'));
    }
    function showPanel() {
      $ctrl.panelModel = newTopic();
      $ctrl.isShowingPanel = true;
    }
    function hidePanel() {
      $ctrl.isShowingPanel = false;
    }
    function savePanel() {
      PostService.insert($ctrl.panelModel)
      .then(function() {
        toastr.info('Guardado Panel');
        $ctrl.isShowingPanel = false;
        return updatePosts();
      })
      .then(null, $rootScope.fcnError('Add Post Panel'));
    }
    function saveDialog(model) {
      PostService.insert(model)
      .then(function() {
        toastr.info('Guardado Dialog');
        return updatePosts();
      })
      .then(null, $rootScope.fcnError('Add Post Dialog'));
    }
    function newTopic() {
      /* jshint bitwise: false */
      return {
        title: null,
        content: null,
        likes: 0,
        createdAt: new Date(),
        topic: $ctrl.topic.id | 0
      };
    }
    function DialogCtrl() {
      var dlg = this;
      dlg.ok = ok;
      dlg.no = no;
      dlg.$onInit = $onInit;

      function $onInit() {
        dlg.model = dlg.$resolve.model;
        dlg.topics = dlg.$resolve.topics;
        dlg.model.topic = dlg.topics.filter(function(t) {
          return t.id === dlg.model.topic.id;
        })[0];
      }
      function ok(result) {
        return dlg.$close(result);
      }
      function no(reason) {
        return dlg.$dismiss(reason);
      }
    }
    function showDialog() {
      var dlgModel = newTopic();
      dlgModel.topic = $ctrl.topic;
      $uibModal.open({
        backdrop: true,
        bindToController: true,
        controller: DialogCtrl,
        controllerAs: 'dlg',
        size: 'lg',
        templateUrl: templateUrl('app/uagrm/view.dialog.html'),
        resolve: {
          model: function() {return dlgModel;},
          topics: resolveTopics
        }
      }).result
      .then(function(result) {
        result.topic = result.topic.id;
        saveDialog(result);
      }, function(reason) {
        console.log({reason:reason});
      });

      function resolveTopics() {
        return TopicService.list();
      }
    }
  }
})();
