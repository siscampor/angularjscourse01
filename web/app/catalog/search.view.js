/**
* Catalog Search Component
**/
(function() {
  'use strict';
  angular.module('app.catalog')
  .component('viewCatalogSearch', {
    templateUrl: templateUrl('app/catalog/search.view.html'),
    controller: SearchCtrl
  });

  SearchCtrl.$inject = ['$rootScope', 'CatalogService', 'CategoryList'];
  function SearchCtrl($rootScope, CatalogService, CategoryList) {
    var $ctrl = this;
    $ctrl.list = [];
    $ctrl.categories = [{
      key: '*',
      val: 'Todas'
    }].concat(CategoryList.map(function(item) {
      return {
        key: item,
        val: item
      };
    }));
    $ctrl.search = {
      title: '',
      author: '',
      category: '*',
      description: ''
    }
    $ctrl.doSearch = doSearch;
    $ctrl.keyPress = keyPress;

    function doSearch() {
      var s = $ctrl.search;
      CatalogService.search(
        s.title,
        s.category,
        s.author,
        s.description
      ).then(function(list) {
        $ctrl.list = list;
      }, $rootScope.fcnError('Buscando Catalogo'));
    }

    function keyPress(e) {
      if (e.keyCode === 13) {
        doSearch();
      }
    }
  }
})();
