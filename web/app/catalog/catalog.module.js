/**
* Catalog Module
**/
(function() {
  'use strict';
  angular.module('app.catalog', ['app.core']);
})();
