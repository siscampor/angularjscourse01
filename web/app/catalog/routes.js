/**
* Catalog Routes
**/
(function() {
  'use strict';
  angular.module('app.catalog')
  .config(CatalogConfig);

  CatalogConfig.$inject = ['stateProvider'];
  function CatalogConfig(stateProvider) {
    stateProvider
    .abs('app.catalog', '/catalog')
    .when('app.catalog.search', '/', {
      template: '<view-catalog-search></view-catalog-search>',
      title: 'Catalogo'
    })
    .when('app.catalog.view', '/:id/view', {
      template: '<view-catalog-view></view-catalog-view>',
      title: 'Detalle'
    })
    .when('app.catalog.edit', '/:id/edit', {
      template: '<view-catalog-edit></view-catalog-edit>',
      title: 'Editar'
    });
  }
})();
