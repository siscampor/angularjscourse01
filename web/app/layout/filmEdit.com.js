/**
* Filp Edit View
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .component('viewFilmEdit', {
    templateUrl: templateUrl('app/layout/filmEdit.comp.html'),
    controller: ViewFilmEditCtrl,
    controllerAs: '$view',
    bindings: {
      film: '<'
    }
  });

  ViewFilmEditCtrl.$inject = ['FilmService', '$state'];
  function ViewFilmEditCtrl(FilmService, $state) {
    var $view = this;
    $view.ratings = 'PG,G,NC-17,PG-13,R'.split(',');
    $view.$onInit = $onInit();
    $view.onSubmit = onSubmit;

    function $onInit() {
    }
    function onSubmit() {
      //$view.film.id = 10023;
      FilmService.update($view.film)
      .then(function() {
        $state.go('app.film.list');
      }, function(err) {
        console.error(err);
      });
    }
  }
})();
