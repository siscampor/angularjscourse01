/**
* Film View component
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .component('viewFilm', {
    templateUrl: templateUrl('app/layout/film.comp.html'),
    controller: ViewFilmCtrl,
    controllerAs: '$view',
    bindings: {
      films: '<'
    }
  });

  function ViewFilmCtrl() {
    var $view = this;
    $view.tc = 6.96;
    $view.limitAll = 30;
    $view.limitSome = 10;
    $view.limitTo = $view.limitSome;
    $view.search = '';
    $view.clsBtnSort = 'btn-success';
    $view.sortName = 'title';
    $view.sortDir = false;
    $view.$onInit = $onInit;

    function $onInit() {
      $view.films.forEach(function(row) {
        row.actors = row.actors.split(', ');
      });
    }
  }
})();
