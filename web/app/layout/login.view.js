/**
* Login Controller
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .component('viewLogin', {
    templateUrl: templateUrl('app/layout/login.view.html'),
    controller: LoginCtrl,
    controllerAs: 'view'
  });

  LoginCtrl.$inject = ['$scope', '$state', 'AuthService'];
  function LoginCtrl($scope, $state, AuthService) {
    var view = this;
    view.username = null;
    view.password = null;
    view.onSubmit = onSubmit;

    function onSubmit(e) {
      if ($scope.frmLogin.$invalid) {return;}
      AuthService.login(view.username, view.password)
      .then(function(data) {
        $scope.$root.setAuth(data);
        $state.go('app.home');
      }, $scope.$root.fcnError('Ingresando'));
    }
  }
})();
