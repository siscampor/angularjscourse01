/**
* My Timer Component
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .component('myTimer', {
    templateUrl: templateUrl('app/layout/myTimer.comp.html'),
    controller: MyTimerCtrl,
    controllerAs: '$ctrl',
    bindings: {
      myMin: '<',
      mySec: '<',
      onTick: '&',
      onStart: '&',
      onStop: '&',
      onReset: '&'
    }
  });

  MyTimerCtrl.$inject = ['$interval'];
  function MyTimerCtrl($interval) {
    var $ctrl = this;
    $ctrl.text = '';
    $ctrl.running = null;
    $ctrl.$onInit = $init;
    $ctrl.$onDestroy = $destroy;
    $ctrl.start = start;
    $ctrl.stop = stop;
    $ctrl.reset = reset;

    function noop() {}
    function $init() {
      console.log({fn:'$init', ctrl:$ctrl});
      if (typeof($ctrl.onTick) !== 'function') {
        $ctrl.onTick = noop;
      }
      if (typeof($ctrl.onStart) !== 'function') {
        $ctrl.onStart = noop;
      }
      if (typeof($ctrl.onStop) !== 'function') {
        $ctrl.onStop = noop;
      }
      if (typeof($ctrl.onReset) !== 'function') {
        $ctrl.onReset = noop;
      }
      $ctrl.min = $ctrl.myMin;
      $ctrl.sec = $ctrl.mySec;
      update();
    }
    function $destroy() {
      stop();
    }
    function pad(num) {
      return (num > 9 ? '' : '0') + num;
    }
    function update() {
      $ctrl.text = [
        pad($ctrl.min),
        pad($ctrl.sec)
      ].join(':');
    }
    function tick() {
      $ctrl.sec += 1;
      while($ctrl.sec >= 60) {
        $ctrl.sec -= 60;
        $ctrl.min += 1;
      }
      while($ctrl.min >= 100) {
        $ctrl.min -= 100;
      }
      update();
      $ctrl.onTick({ctrl:$ctrl});
    }
    function start() {
      if ($ctrl.isRunning) {return;}
      $ctrl.running = $interval(tick, 1000);
      $ctrl.onStart({ctrl:$ctrl});
    }
    function stop() {
      if (!$ctrl.running) {return;}
      $interval.cancel($ctrl.running);
      $ctrl.running = null;
      $ctrl.onStop({ctrl:$ctrl});
    }
    function reset() {
      stop();
      $ctrl.min = $ctrl.myMin;
      $ctrl.sec = $ctrl.mySec;
      update();
      $ctrl.onReset({ctrl:$ctrl});
    }
  }
})();
