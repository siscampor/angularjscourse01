/**
* Home Controller
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .component('viewHome', {
    templateUrl: templateUrl('app/layout/home.view.html'),
    controller: ViewHomeCtrl,
    controllerAs: 'view',
    bindings: {
      topics: '<'
    }
  });

  ViewHomeCtrl.$inject = ['$rootScope', 'AuthService', '$interval'];
  function ViewHomeCtrl($rootScope, AuthService, $interval) {
    var view = this;
    view.search = 'Hola';
    view.selected = null;
    view.auth = AuthService;
    view.onSearchChange = onSearchChange;
    view.min = 2;
    view.sec = 30;
    view.logs = [];
    view.onTick = onTick;
    view.onStart = onStart;
    view.onStop = onStop;
    view.onReset = onReset;
    view.$onInit = $onInit;
    view.onFilmGet = onFilmGet;

    function $onInit() {
      $rootScope.loadedRoute();
    }
    function onFilmGet() {
      /* jshint devel: true */
      AuthService.$http.get(AuthService.url('..', 'film'))
      .then(function(response) {
        console.log('film', response.data);
      }, function(err) {
        console.error(err);
      });
    }
    function onSearchChange(topic) {
      view.selected = null;
      view.topics.forEach(function(topic) {
        if (view.search === topic.title) {
          view.selected = topic.title;
        }
      });
    }
    function log(msg) {
      var now  = new Date();
      view.logs.unshift({
        id: view.logs.length,
        date: now,
        msg: msg
      });
    }
    function onTick(ctrl) {
      //log('onTick: ' + ctrl.text);
    }
    function onStart(ctrl) {
      //log('onStart: ' + ctrl.text);
    }
    function onStop(ctrl) {
      log('onStop: ' + ctrl.text);
    }
    function onReset(ctrl) {
      //log('onReset: ' + ctrl.text);
    }
  }
})();
