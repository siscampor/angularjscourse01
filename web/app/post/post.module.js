/**
* Post Module
**/
(function() {
  'use strict';
  angular.module('app.post', ['app.core']);
})();
