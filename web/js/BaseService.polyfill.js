/**
* Base Service
**/
(function() {
  'use strict';
  window.BaseService = BaseService;
  function BaseService($http, env, urls) {
    if (!Array.isArray(urls)) {
      urls = [].slice.call(arguments, 2);
    }
    this.$http = $http;
    this.env = env;
    if (!$http.del) {
      $http.del = $http['delete'];
    }
    this.urls = this.getUrlRoot(urls);
  }
  var pBase = BaseService.prototype;
  pBase.url = function(args) {
    if (!Array.isArray(args)) { args = [].slice.call(arguments); }
    var urls = this.urls.concat(args);
    return urls.join('/');
  };
  pBase.getUrlRoot = function(urls) {
    if (!Array.isArray(urls)) {
      urls = [].slice.call(arguments);
    }
    urls.unshift(this.env.apiUrl);
    return urls;
  };
  pBase.fcnUrlRoot = function() {
    var root = this.getUrlRoot([].slice.call(arguments));
    return function(args) {
      if (!Array.isArray(args)) { args = [].slice.call(arguments); }
      var urls = root.concat(args);
      return urls.join('/');
    };
  };
  pBase.clone = function(obj) {
    if (obj === null) {return null;}
    if (obj === undefined) { return undefined; }
    return JSON.parse(JSON.stringify(obj));
  };
  pBase.dataData = function(data) {
    return data.data;
  };
  window.ModelService = ModelService;
  function ModelService($http, env, urls) {
    if (!Array.isArray(urls)) {
      urls = [].slice.call(arguments, 2);
    }
    BaseService.call(this, $http, env, urls);
  }
  ModelService.prototype = Object.create(BaseService.prototype);
  ModelService.prototype.constructor = ModelService;
  var pModel = ModelService.prototype;
  pModel.list = function() {
    return this.$http.get(this.url()).then(this.dataData);
  };
  pModel.findById = function(id) {
    return this.$http.get(this.url(id)).then(this.dataData);
  };
  pModel.insert = function(model) {
    return this.$http.post(this.url(), model).then(this.dataData);
  };
  pModel.update = function(model) {
    return this.$http.put(this.url(model.id), model).then(this.dataData);
  };
  pModel.del = function(id) {
    return this.$http.del(this.url(id)).then(this.dataData);
  };
})();
