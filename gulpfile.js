'use strict';
var args = require('yargs').argv;
var now = new Date();
var pad = function(num) {
  return (num <= 9 ? '0':'') + num;
};
var nowStr = [
  now.getFullYear(),
  pad(now.getMonth()+1),
  pad(now.getDate()),
  pad(now.getHours()),
  pad(now.getMinutes()),
  pad(now.getSeconds())
].join('');
var gulp = require('gulp');
var config = {
  src: './web/',
  dst: './www/',
  tmp: './.tmp/',
  last: 'www-inject',
  now: nowStr,
  args: args,
  optimize: !!args.optimize || !!args.prod || !!args.test,
  verbose: !!args.verbose,
  profile: args.profile ? args.profile : args.prod ? 'prod' : args.test ? 'test' : 'dev',
  path: args.path ? args.path : ''
};
var libs = {
  autoprefixer:  require('gulp-autoprefixer'),
  concat: require('gulp-concat'),
  cssnano: require('gulp-cssnano'),
  del: require('del'),
  deleteEmpty: require('delete-empty'),
  fs: require('fs'),
  gif: require('gulp-if'),
  htmlMin: require('gulp-htmlmin'),
  image:  require('gulp-image'),
  inject: require('gulp-inject'),
  jshint: require('gulp-jshint'),
  less: require('gulp-less'),
  ngConfig: require('gulp-ng-config'),
  plumber: require('gulp-plumber'),
  print: require('gulp-print'),
  rename: require('gulp-rename'),
  runSequence: require('run-sequence'),
  taskListing: require('gulp-task-listing'),
  sourcemaps: require('gulp-sourcemaps'),
  stylish: require('jshint-stylish'),
  templateCache: require('gulp-angular-templatecache'),
  uglify: require('gulp-uglify'),
  using: require('gulp-using'),
  wiredep: require('wiredep').stream
};
gulp.task('default', ['help']);
gulp.task('help', libs.taskListing);
gulp.task('www', [
  'www-clean',
  'www-index',
  'www-resources',
  'www-code',
  'www-inject',
  'www-index-args'
]);
require('./gulp/index.js')(gulp, config, libs);
require('./gulp/resources.js')(gulp, config, libs);
require('./gulp/code.js')(gulp, config, libs);
require('./gulp/inject.js')(gulp, config, libs);
require('./gulp/env.js')(gulp, config, libs);
