'use strict';
// Ensure we're in the project directory, so relative paths work as expected
// no matter where we actually lift from.
process.chdir(__dirname);
var http = require('http');

var app = {
  root: __dirname,
  port: 3000,
  config: {},
};

require('./srv')(app);
http.createServer(app.web).listen(app.port, started);

function started(e) {
  if (e) {
    console.error(e);
  } else {
    console.log('App running in port ' + app.port);
  }
}
